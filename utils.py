#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

from concurrent.futures import ThreadPoolExecutor
from random import randrange, seed
from urllib.request import urlopen
import json
import os
import re
import time


# This will create alternative universes where things happen in parallel
# When all results have been obtained from them, this timeline continues
def create_multiverse(tasks):
    running_tasks = []
    results = []
    with ThreadPoolExecutor() as executor:
        for task in tasks:
            [function, argument] = task
            running_tasks.append(executor.submit(function, argument))
        for running_task in running_tasks:
            results.append(running_task.result())
    return results


# This will create a new run with a new directory for all the run details
# the latest_run_id file will contain the id for the directory
# so that computation can be resumed later
def create_new_run_id():
    run_id = "./run_{}".format(int(time.time()))
    print("Starting new experiment in {}".format(run_id))
    with open("latest_run_id", "w") as id_file:
        id_file.write(run_id)
    os.mkdir("./{}".format(run_id))
    return run_id


def get_instances(run_id=None):
    if run_id is None:
        run_id = read_ceremony_id()
    instances = read_from_file("{}/{}".format(run_id, "instances.json"))
    return instances


def read_ceremony_id():
    with open("latest_run_id", "r") as id_file:
        return id_file.readline()


def write_to_file(in_dict, filename):
    with open(filename, "w") as file_to_write:
        file_to_write.write(json.dumps(in_dict, indent=4, sort_keys=True, default=str))


def read_from_file(filename):
    with open(filename, "r") as file_to_read:
        return json.loads(file_to_read.read())


def get_random_topology(num_nodes, num_connections=8):

    # we want deterministic randomness so that results are reproducible
    seed(1)

    def get_random_candidate(from_node):
        while True: 
            try_this = randrange(num_nodes)
            if try_this != from_node and [from_node, try_this] not in existing_edges:
                return try_this

    # If all nodes are reachable from a source node then it is fully connected
    def check_node_fully_connected(origin):
        visited = set()
        to_visit = set()

        to_visit.add(origin)

        def get_connected_to(from_node):
            to_nodes = []
            for edge in existing_edges:
                if (edge[0] == from_node):
                    to_nodes.append(edge[1])
            return to_nodes
            
        while len(to_visit) != 0:
            current_node = to_visit.pop()
            visited.add(current_node)
            for to_node in get_connected_to(current_node):
                if to_node not in visited:
                    to_visit.add(to_node)
        return len(visited) == num_nodes

    # If all nodes are fully connected then the network is fully connected
    def check_network_fully_connected():
        for i in range(num_nodes):
            if not check_node_fully_connected(i):
                return False
        return True

    fully_connected = False  # if after a full iteration the net not fully connected, simply start over until it is
    iterations = 0
    while not fully_connected:
        existing_edges = []
        for from_node in range(num_nodes):
            for j in range(num_connections):
                to_node = get_random_candidate(from_node)
                existing_edges.append([from_node, to_node])
        iterations = iterations+1
        fully_connected = check_network_fully_connected()

    print("Costructed random network in {} iterations".format(iterations))
    print(existing_edges)

    return existing_edges


def get_public_ip():
    data = str(urlopen('http://checkip.dyndns.com/').read())
    # data = '<html><head><title>Current IP Check</title></head><body>Current IP Address: 65.96.168.198</body></html>\r\n'

    return re.compile(r'Address: (\d+\.\d+\.\d+\.\d+)').search(data).group(1)
