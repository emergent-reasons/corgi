#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

import os
import random

import boto3

import configs
import utils


images = dict()
permissions = [
                {
                    "FromPort": 22,
                    "ToPort": 22,
                    "IpProtocol": "tcp",
                    "IpRanges": [{"CidrIp" :"0.0.0.0/0", "Description": "SSH"}]
                },
                {
                    "FromPort": 8332,
                    "ToPort": 8333,
                    "IpProtocol": "tcp",
                    "IpRanges": [{"CidrIp" :"0.0.0.0/0", "Description": "Bitcoin+RPC"}]
                }
            ]


# create the aws clients
def create_aws_regional_clients():
    try:
        dummy_client = boto3.client("ec2", "eu-central-1")
    except Exception as error:
        print("Probable error: AWS credentials not found. Check https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html#guide-configuration")
        print(error)
        raise    
    
    descr = dummy_client.describe_regions()
    print("Got a total of {} regions".format(len(descr["Regions"])))
    
    regional_clients = dict()

    def open_portal(region_name):
        regional_clients[region_name] = boto3.client("ec2", region_name)
        # The same base images in diffrent regions have different ids
        i = regional_clients[region_name].describe_images(Filters=[{"Name":"name", "Values":[configs.image_name]}])
        images[region_name] = i["Images"][0]["ImageId"]

    tasks = []
    for region in descr["Regions"]:
        region_name = region["RegionName"]
        tasks.append([open_portal, region_name])
    utils.create_multiverse(tasks)

    print("{} regional_clients created".format(len(regional_clients)))

    return regional_clients


def create_security_groups(regional_clients, run_id, security_groups):

    def create_security_group(region_name):
        # keypairs and security_groups (for ssh) are valid only for one region, so we create one for each
        # we"ll use a new one each run, so deleting the old one called "ceremony"
        # remeber that you did agree to the MIT above
        # crazy shit happens == you did it to yourself
        keypairs = dict()
        regional_clients[region_name].delete_key_pair(KeyName="ceremony")
        keypair = regional_clients[region_name].create_key_pair(KeyName="ceremony")
        keypairs[region_name] = keypair
        privkeyfilename = "{}/{}.pem".format(run_id, region_name)
        with open(privkeyfilename, "w") as privkeyfile:
            privkeyfile.write(str(keypair["KeyMaterial"]))
        os.chmod(privkeyfilename, 0o400)
        try: 
            regional_clients[region_name].delete_security_group(GroupName="ceremony")
        except: 
            pass
        # if the following fails, you probably have some dangling instances running
        # remove them manually or unleash the armageddon()
        security_group = regional_clients[region_name].create_security_group(Description="ceremony", GroupName="ceremony")
        response = regional_clients[region_name].authorize_security_group_ingress(
            GroupId=security_group["GroupId"],
            IpPermissions=permissions)
        security_groups[region_name] = security_group["GroupId"]
    
    print("Creating security groups")

    tasks = []
    for region_name in regional_clients:
        tasks.append([create_security_group, region_name])
    utils.create_multiverse(tasks)

    print("Security groups created")


# prepare the instances
def create_aws_instances(run_id, regional_clients, security_groups, num_nodes):

    instances = []
    detailed_instances = []
    print("Creating {} nodes around the world".format(num_nodes))

    while len(instances) < num_nodes:
        try:
            region_name = random.choice(list(regional_clients.keys()))
            response = regional_clients[region_name].run_instances(
                ImageId=images[region_name],
                InstanceType="t3.micro",
                KeyName="ceremony",
                SecurityGroupIds=[security_groups[region_name]],
                CreditSpecification={"CpuCredits": "standard"}, # this will save some cents on a short-running instance
                MinCount=1,
                MaxCount=1)
            print("Created instance: {} in region {}".format(response["Instances"][0]["InstanceId"], region_name))
            instances.append({"region": region_name, 
                "id": response["Instances"][0]["InstanceId"],
                })
            detailed_instances.append(response["Instances"][0])   # for simplicity, this will have it's own file

        except Exception as e:
            print("Exception in region {}: {}".format(region_name, e))

    # wait for the instances to be up and running
    print("Waiting for the instances to startup (to get their public IPs)")
    for instance in instances:
        region_name = instance["region"]
        waiter = regional_clients[region_name].get_waiter("instance_running")
        waiter.wait(InstanceIds=[instance["id"]])
    
    # finally the public ips should be available
    for instance in instances:
        region_name = instance["region"]
        description = regional_clients[region_name].describe_instances(InstanceIds=[instance["id"]])
        instance["ip"] = description["Reservations"][0]["Instances"][0]["PublicIpAddress"]

    # Neat log summing it all up
    for instance in instances:
        print(instance)

    utils.write_to_file(instances, "{}/{}".format(run_id, "instances.json"))
    utils.write_to_file(detailed_instances, "{}/{}".format(run_id, "detailed_instances.json"))
