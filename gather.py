#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

import os

import utils


# get all the logs form the nodes
def gather(run_id=None):
    if run_id is None:
        run_id = utils.read_ceremony_id()        
    instances = utils.get_instances(run_id)

    def copy_to_local(instance, filename, localfilename=None):
        if localfilename is None:
            localfilename = filename
        command = "scp -oStrictHostKeyChecking=no -i {} ubuntu@{}:~/{} {}/{}-{}-{}".format(
            keyfile, 
            ip, 
            filename,
            run_id,
            instance["region"], 
            instance["id"],
            filename)
        os.system(command)

    for instance in instances:
        ip = instance["ip"]
        region_name = instance["region"]
        keyfile = "{}/{}.pem".format(run_id, region_name)

        copy_to_local(instance, "hashtx.log")
        copy_to_local(instance, "hashblock.log")
        copy_to_local(instance, ".bitcoin/regtest/debug.log", "debug.log")
        
        # Uncomment this if you also want to check the script logs         
        # copy_to_local(instance, "script.log")


# gather()
