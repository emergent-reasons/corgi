#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

# Use this, this will terminate instances started by this script
# Did I already mention that I"m not responsible for your bills?
# If anything fails it"s your responsaiblity to shut your instances off

import configs
import prepare_aws
import utils


def cleanup(run_id=None):
    regional_clients = prepare_aws.create_aws_regional_clients()
    instances = utils.get_instances(run_id)

    for instance in instances:
        try:
            print("Terminating instance {} in region {}".format(instance["id"], instance["region"]))
            regional_clients[instance["region"]].terminate_instances(InstanceIds=[instance["id"]])
        except Exception as e:
            print("Instance {} in region {} failed to stop with error {}".format(instance["id"], instance["region"], e))


# Don't use this, it will end the world
def armageddon():
    if configs.i_accept_the_responsibility_of_running_this_script != True:
        raise Exception("With great power comes great responsibility. Do you accept it?") 

    regional_clients = prepare_aws.create_aws_regional_clients()
    for region_name in regional_clients:
        response = regional_clients[region_name].describe_instances()
        for reservation in response["Reservations"]:
            for instance in reservation["Instances"]:
                try:
                    print("Terminating instance {} in region {}".format(instance["InstanceId"], region_name))
                    regional_clients[region_name].terminate_instances(InstanceIds=[instance["InstanceId"]])
                except Exception as e:
                    print("Instance {} in region {} failed to stop with error {}".format(instance.id, instance.region, e))                    

