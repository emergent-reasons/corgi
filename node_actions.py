#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.


# This is where a big transaction spam to test the network's properties should be done
# It is currently not implemented

import time

from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException


def send_transactions_continuosly():
    rpc_user = "vestal"
    rpc_password = "1qaz2wsx3edc4rfv5tgb"
    url = "http://{}:{}@{}:8332".format(rpc_user, rpc_password, "127.0.0.1")
    rpc_connection = AuthServiceProxy(url)

    address = rpc_connection.getnewaddress()

    while True:
        print(rpc_connection.sendtoaddress(address, "0.001", "", "", False))
        time.sleep(10)


send_transactions_continuosly()
