![Corgi Logo](Corgi_128.png)

Introduction
=====

Corgi is a small and cute [herding dog](https://en.wikipedia.org/wiki/Welsh_Corgi)

It is also the name of a disrtributed framework for testing emergent properties of `bitcoind` networks, and will effectively manage a big herd of nodes. It's meant to be lean, small, pleasant and fun.

Things like propagation times for transactions or blocks, block compression algorithms, behaviour under high congestion, pre-consensus algorithms and other stuff could be tested. 

Rationale
=========

The idea of this project was born on BCHN Slack, discussing transaction relay strategies. I personally have an inclination for evidence based development. Science is science if it's rooted in experience. 

I think that there are probably quite a few frameworks similar to this (it's certainly not the first effort for distributed testing), but I wanted something that's easy to explain, easy to understand and easy to run. I think it's best if anyone and everyone can reproduce the findings of any research. 

Workflow
========

Firstly, there are some preliminary checks and some housekeeping is done: VPSs are created and equipped with bitcoind.

Then, the actual test at hand is executed. Ongoing status for each bitcoind node is logged on each VPS locally. 

Then, the results of the tests are pulled from the VPSs

They are then processed and transformed to some human-readable form

Lastly, the VPSs are terminated. 

Running details
===============

You can install python requirements with `requirements.txt`, typically:

* `pip install --user -r requirements.txt`

On first run, the framework will create a file `latest_run_id`, and it's content is simply the name of the directory where this experiment's data will be stored. The data includes

* private keys to access the VPSs in ssh
* `instances.json` which holds the IPs of the instances
* experiment's results
* processing

If you halt a run mid-flight, you can just restart it. The `latest_run_id` will be read, and all the necessary data from that. This is useful, because the experiments will probably need some manual fiddling, can't expect to get *everything* right the first time round. Or if your internet acts up.

At the end the instances can be terminated. 

If something goes wrong, there is the option of `armageddon()`. It will terminate all your instances in all regions, no matter if they're relative to this framework or not. You shouldn't ever use it. 

Costs
=====

By running this you will incur in some costs. It's proportional to the amount of VPSs you choose to run, which type of instance and for how long. 

```
Sixteen t3.micro instances running for 2 hours cost me some $1

YMMV
```

If you also choose to run the spam test, you will obviously need to pay for the transaction costs. 

DANGER
======

The framework includes scripts for terminating VPS instances. There is an obvious risk of catastrophic loss in case something goes bad. 

If you use the same aws account for other work, don't.

You are solely responsible for running these scripts. 

Results
=======

This repo stores primarily the code, but I'll also put results with some explanation as research is done. 

TODO
=======

TODO list for initial release

```
[X] basic setup
[X] setup boto3/aws connections
[X] create vps server instances
[½] setup bchn nodes on each instance
[x] download a snapshot of a pruned blockchain
[x] run the node
[x] startup a python script to collect fine grained timings on recieved transactions
[x] propagte a whole bunch of transactions through the nodes
[x] copy the logs back to local
[x] process the logs, make a tidy csv
[ ] make some juicy charts out of it (WIP)
[x] terminate the aws instances
[x] optional: terminate all existing aws instances for this account (DANGER)
```
